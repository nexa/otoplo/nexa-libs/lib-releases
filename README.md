## Maven

For maven packages (nexa4j, rostrum4j) add the following to your pom.xml and replace the VERSION value with latest release:

```
<repositories>
    <repository>
        <id>gitlab-maven</id>
        <url>https://gitlab.com/api/v4/projects/44638913/packages/maven</url>
    </repository>
</repositories>

<dependency>
    <groupId>com.otoplo</groupId>
    <artifactId>nexa4j</artifactId>
    <version>{VERSION}</version>
</dependency>

<dependency>
    <groupId>com.otoplo</groupId>
    <artifactId>rostrum4j</artifactId>
    <version>{VERSION}</version>
</dependency>
```